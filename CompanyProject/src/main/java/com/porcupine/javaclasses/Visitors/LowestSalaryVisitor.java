package com.porcupine.javaclasses.Visitors;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

import java.util.Optional;


public class LowestSalaryVisitor implements EmployeeVisitor {

    private Employee lowestSalaryEmployee;

    @Override
    public void visit(Employee employee) {

        lowestSalaryEmployee = (lowestSalaryEmployee == null || lowestSalaryEmployee.getSalary() < employee.getSalary()) ?
                employee : lowestSalaryEmployee;
    }

    @Override
    public void visit(Manager manager) {

        lowestSalaryEmployee = (lowestSalaryEmployee == null || manager.getSalary() < lowestSalaryEmployee.getSalary()) ?
                manager : lowestSalaryEmployee;

        Optional <Employee> tmp =
                manager.getEmployeeList()
                .stream()
                .map(e -> {

                    LowestSalaryVisitor lsv = new LowestSalaryVisitor();
                    e.accept(lsv);
                    return lsv.getLowestSalaryEmployee();
                })
                .min((e1, e2) -> (e1.getSalary() > e2.getSalary()) ? 1 : -1);

        lowestSalaryEmployee = (tmp.isPresent() && tmp.get().getSalary() < lowestSalaryEmployee.getSalary()) ?
                tmp.get() : lowestSalaryEmployee;
    }

    public Employee getLowestSalaryEmployee() {
        return lowestSalaryEmployee;
    }
}
