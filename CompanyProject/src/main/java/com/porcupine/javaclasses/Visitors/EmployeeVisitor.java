package com.porcupine.javaclasses.Visitors;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

public interface EmployeeVisitor {

    void visit(Employee employee);
    void visit(Manager manager);
}
