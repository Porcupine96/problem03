package com.porcupine.javaclasses.Visitors;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

import java.util.HashSet;
import java.util.Set;

public class UnsatisfiedEmployeesVisitor implements EmployeeVisitor{

    private Set<Employee> unsatisfiedEmployees;

    public UnsatisfiedEmployeesVisitor() {
        unsatisfiedEmployees = new HashSet<>();
    }

    @Override
    public void visit(Employee employee) {
        if (!employee.getSatisfaction()) unsatisfiedEmployees.add(employee);
    }

    @Override
    public void visit(Manager manager) {

        if (!manager.getSatisfaction()) unsatisfiedEmployees.add(manager);

        manager.getEmployeeList()
        .stream()
        .forEach(e -> {

            UnsatisfiedEmployeesVisitor nsev = new UnsatisfiedEmployeesVisitor();
            e.accept(nsev);
            unsatisfiedEmployees.addAll(nsev.unsatisfiedEmployees);
        });
    }

    public Set<Employee> getUnsatisfiedEmployees() {
        return unsatisfiedEmployees;
    }
}
