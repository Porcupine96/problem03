package com.porcupine.javaclasses.Visitors;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

public class SalarySumVisitor implements EmployeeVisitor {

    private int salarySum;

    @Override
    public void visit(Employee employee) {
        salarySum += employee.getSalary();
    }

    @Override
    public void visit(Manager manager) {

        salarySum += (manager.getEmployeeList()
                .stream()
                .mapToInt(e -> {

                    SalarySumVisitor salarySumVisitor = new SalarySumVisitor();
                    e.accept(salarySumVisitor);
                    return salarySumVisitor.getSalarySum();
                })
                .sum()
                + manager.getSalary());
    }

    public int getSalarySum() {
        return salarySum;
    }
}
