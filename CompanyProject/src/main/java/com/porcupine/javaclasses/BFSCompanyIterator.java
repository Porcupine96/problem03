package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Predicate;

public class BFSCompanyIterator implements Iterator<Employee> {

    private Queue<Employee> queue;
    private Predicate<Employee> predicate;

    public BFSCompanyIterator(Predicate<Employee> predicate) {

        this.queue = new LinkedList<>();
        this.predicate = predicate;

        Manager ceo = Company.getInstance().getCEO();
        if (predicate.test(ceo)) queue.offer(ceo);
        else getFirstToTheFront(ceo);
    }

    private void getFirstToTheFront(Manager ceo) {

        ceo.getEmployeeList()
                .forEach(queue::offer);

        Employee tmp = queue.peek();

        while (tmp != null && !predicate.test(tmp)) {

            queue.poll();
            if (tmp instanceof Manager)
                ((Manager) tmp).getEmployeeList()
                        .forEach(queue::offer);

            tmp = queue.peek();
        }
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }

    @Override
    public Employee next() {

        Employee validEmployee = queue.peek();
        Employee tmp = validEmployee;

        do {
            queue.poll();
            if (tmp instanceof Manager)
                ((Manager) tmp).getEmployeeList()
                        .forEach(queue::offer);

            tmp = queue.peek();

        } while(tmp != null && !predicate.test(tmp));

        return validEmployee;
    }
}
