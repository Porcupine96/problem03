package com.porcupine.javaclasses.Staff;

import com.porcupine.javaclasses.Strategies.HiringStrategy;
import com.porcupine.javaclasses.Visitors.EmployeeVisitor;

import java.util.ArrayList;
import java.util.List;

abstract public class Manager extends Employee {

    private List<Employee> employeeList;
    private HiringStrategy hiringStrategy;

    public Manager(String name, int salary, HiringStrategy hiringStrategy) {
        super(name, salary);
        this.employeeList = new ArrayList<>();
        this.hiringStrategy = hiringStrategy;
    }

    public String hireAnEmployee(Employee employee) {

        if (getHiringStrategy().canHire(this, employee)) {
            employeeList.add(employee);
            return "Welcome on board " + employee.getName() + "!\n";
        }
        return "Sorry, but I can't hire you.\n";
    }
//
//    public void addEmployee(Employee employee) {
//        this.employeeList.add(employee);
//    }

    public List<Employee> getEmployeeList() {
        // we don't want some naughty external class to mess up with the original employeeList
        return new ArrayList <> (employeeList);
    }

    public HiringStrategy getHiringStrategy() {
        return hiringStrategy;
    }

    public void accept(EmployeeVisitor visitor) { visitor.visit(this); }

    @Override
    public String toString() {
        return getName() + " - Manager";
    }
}
