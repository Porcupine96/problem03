package com.porcupine.javaclasses.Staff;

import com.porcupine.javaclasses.Strategies.CounterOrientedStrategy;

public class CounterOrientedManager extends Manager {

    private int counter;

    public CounterOrientedManager(String name, int salary, int counter) {
        super(name, salary, new CounterOrientedStrategy());
        this.counter = counter;
    }

    @Override
    public String hireAnEmployee(Employee employee) {
        if (getHiringStrategy().canHire(this, employee)) counter--;
        return super.hireAnEmployee(employee);
    }

    public int getCounter() {
        return counter;
    }
}
