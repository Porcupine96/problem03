package com.porcupine.javaclasses.Staff;

import com.porcupine.javaclasses.Strategies.BudgetOrientedStrategy;

public class BudgetOrientedManager extends Manager{

    private int budget;

    public BudgetOrientedManager(String name, int salary, int budget) {
        super(name, salary, new BudgetOrientedStrategy());
        this.budget = budget;
    }

    @Override
    public String hireAnEmployee(Employee employee) {
        if (getHiringStrategy().canHire(this, employee)) budget -= employee.getSalary();
        return super.hireAnEmployee(employee);
    }

    public int getBudget() {
        return budget;
    }
}
