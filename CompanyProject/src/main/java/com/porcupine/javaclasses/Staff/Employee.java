package com.porcupine.javaclasses.Staff;

import com.porcupine.javaclasses.Visitors.EmployeeVisitor;

public class Employee {

    private final String name;
    private int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    // we could change the salary later on
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public String salaryAnswer() {
        return "I earn " + salary + "!\n";
    }

    public Boolean getSatisfaction() {
        return salary > 10000;
    }

    public void accept(EmployeeVisitor visitor) { visitor.visit(this); }

    @Override
    public String toString() {
        return name + " - Employee";
    }
}
