package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.*;
import com.porcupine.javaclasses.Visitors.LowestSalaryVisitor;
import com.porcupine.javaclasses.Visitors.UnsatisfiedEmployeesVisitor;
import com.porcupine.javaclasses.Visitors.SalarySumVisitor;

import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;

public class Company implements Iterable<Employee> {


    // I thought about making it Optional but I've read that it's a bad practice to make class fields Optional
    private Manager ceo;
    private static Company company;

    private Company() {}

    public static Company getInstance() {

        if (company == null)
            company = new Company();

        return company;
    }

    public void hireCEO(Manager ceo) {
        this.ceo = ceo;
    }


    public Employee getEmployeeWithLowestSalary() {

        LowestSalaryVisitor lsv = new LowestSalaryVisitor();
        ceo.accept(lsv);
        return lsv.getLowestSalaryEmployee();
    }

    public Set<Employee> getUnsatisfiedEmployees() {

        UnsatisfiedEmployeesVisitor nsev = new UnsatisfiedEmployeesVisitor();
        ceo.accept(nsev);
        return nsev.getUnsatisfiedEmployees();
    }

    public int getSumOfSalaries() {

        SalarySumVisitor ssv = new SalarySumVisitor();
        ceo.accept(ssv);
        return ssv.getSalarySum();
    }

    @Override
    public String toString() { // TODO: 07/05/16 It works but it needs optimisation - Ugliness

        if (ceo == null) return "No one has been hired!\n";
        String companyStructure = ceo.getName() + " - CEO\n";

        StringBuilder result = new StringBuilder(companyStructure);

        for (Employee e : ceo.getEmployeeList()) {

            if (e.getClass() == BudgetOrientedManager.class || e.getClass() == CounterOrientedManager.class) {

                result.append("\t" + e.getName() + " - Manager \n");

                for (Employee f : ((Manager)e).getEmployeeList()) {
                    result.append("\t\t" + f.getName() + " - Employee \n");
                }
            }
            else {
                result.append("\t" + e.getName() + " - Employee \n");
            }
        }
        companyStructure = result.toString();

        return companyStructure;
    }

    @Override
    public Iterator<Employee> iterator() {
        return new BFSCompanyIterator(e -> true);
    }

    public Iterator<Employee> iterator(Predicate<Employee> predicate) {
        return new BFSCompanyIterator(predicate);
    }

    public Manager getCEO() {
        return ceo;
    }
}
