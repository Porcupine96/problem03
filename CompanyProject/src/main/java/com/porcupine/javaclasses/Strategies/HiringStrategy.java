package com.porcupine.javaclasses.Strategies;

import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

public interface HiringStrategy {

    boolean canHire(Manager manager, Employee employee);
}
