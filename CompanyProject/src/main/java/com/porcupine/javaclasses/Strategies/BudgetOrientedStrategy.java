package com.porcupine.javaclasses.Strategies;

import com.porcupine.javaclasses.Staff.BudgetOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

public class BudgetOrientedStrategy implements HiringStrategy{

    @Override
    public boolean canHire(Manager manager, Employee employee) {
        return getSumOfEmployeesSalaries(manager) + employee.getSalary() <= ((BudgetOrientedManager) manager).getBudget();
    }

    private int getSumOfEmployeesSalaries(Manager manager) {

        return manager.getEmployeeList()
                        .stream()
                        .mapToInt(Employee::getSalary)
                        .sum();
    }
}
