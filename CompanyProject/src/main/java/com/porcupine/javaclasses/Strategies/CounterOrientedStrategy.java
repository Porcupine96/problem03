package com.porcupine.javaclasses.Strategies;

import com.porcupine.javaclasses.Staff.CounterOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;

public class CounterOrientedStrategy implements HiringStrategy {

    @Override
    public boolean canHire(Manager manager, Employee employee) {
        return ((CounterOrientedManager) manager).getCounter() > 0;
    }
}