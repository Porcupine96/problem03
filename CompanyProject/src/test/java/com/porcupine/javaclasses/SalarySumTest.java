package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.BudgetOrientedManager;
import com.porcupine.javaclasses.Staff.CounterOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SalarySumTest {

    private Company company;

    @Before
    public void setUp() {

        company = Company.getInstance();

        Manager jack = new BudgetOrientedManager("Jack", 11000, 20000);
        company.hireCEO(jack);

        Manager john = new BudgetOrientedManager("John", 3500, 10000);
        Manager sue = new CounterOrientedManager("Sue", 4000, 5);
        Employee ellen = new Employee("Ellen", 12000);
        Employee giorgia = new Employee("Giorgia", 3000);
        Employee fred = new Employee("Fred", 1232);

        john.hireAnEmployee(giorgia);
        sue.hireAnEmployee(ellen);
        sue.hireAnEmployee(fred);

        jack.hireAnEmployee(john);
        jack.hireAnEmployee(sue);
    }

    @Test
    public void shouldReturnEmployeeWithTheLowestSalary() {
        assertEquals(34732, company.getSumOfSalaries());
    }
}
