package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LowestSalaryTest {

    private Company company;

    @Before
    public void setUp() {

        company = Company.getInstance();

        Manager jack = new BudgetOrientedManager("Jack", 10000, 20000);
        company.hireCEO(jack);

        Manager john = new BudgetOrientedManager("John", 3500, 10000);
        Manager sue = new CounterOrientedManager("Sue", 4000, 5);
        Employee ellen = new Employee("Ellen", 2000);
        Employee giorgia = new Employee("Giorgia", 3000);
        Employee fred = new Employee("Fred", 1232);

        john.hireAnEmployee(giorgia);
        sue.hireAnEmployee(ellen);
        sue.hireAnEmployee(fred);

        jack.hireAnEmployee(john);
        jack.hireAnEmployee(sue);
    }

    @Test
    public void shouldReturnEmployeeWithTheLowestSalary() {

        assertEquals(1232, company.getEmployeeWithLowestSalary().getSalary());
        assertEquals("Fred", company.getEmployeeWithLowestSalary().getName());
    }


}
