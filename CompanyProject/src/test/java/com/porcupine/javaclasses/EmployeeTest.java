package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmployeeTest {

    private Employee mark;
    private Employee frank;

    @Before
    public void setUp() {
        mark = new Employee("Mark", 2000);
        frank = new Employee("Frank", 11000);
    }

    @Test
    public void shouldReturnProperName() {

        assertEquals("Mark", mark.getName());
        assertEquals("Frank", frank.getName());
    }

    @Test
    public void shouldChangeSalary() {

        mark.setSalary(5000);
        assertEquals(5000, mark.getSalary());
    }

    @Test
    public void shouldReturnProperSalary() {

        assertEquals(2000, mark.getSalary());
        assertEquals(11000, frank.getSalary());
        mark.setSalary(5000);
        assertEquals(5000, mark.getSalary());

    }
    @Test
    public void shouldAnswerAboutSalary() {

        assertEquals("I earn 2000!\n", mark.salaryAnswer());
        assertEquals("I earn 11000!\n", frank.salaryAnswer());
    }

    @Test
    public void shouldReturnSatisfactionLevel() {

        assertEquals(false, mark.getSatisfaction());
        assertEquals(true, frank.getSatisfaction());
        mark.setSalary(15000);
        assertEquals(true, mark.getSatisfaction());
    }
}
