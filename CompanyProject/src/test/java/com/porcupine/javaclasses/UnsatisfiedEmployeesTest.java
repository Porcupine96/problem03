package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Company;
import com.porcupine.javaclasses.Staff.BudgetOrientedManager;
import com.porcupine.javaclasses.Staff.CounterOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class UnsatisfiedEmployeesTest {

    private Company company;

    @Before
    public void setUp() {

        company = Company.getInstance();

        Manager jack = new BudgetOrientedManager("Jack", 11000, 20000);
        company.hireCEO(jack);

        Manager john = new BudgetOrientedManager("John", 3500, 10000);
        Manager sue = new CounterOrientedManager("Sue", 4100, 5);
        Employee ellen = new Employee("Ellen", 12000);
        Employee giorgia = new Employee("Giorgia", 3000);
        Employee fred = new Employee("Fred", 1232);

        john.hireAnEmployee(giorgia);
        sue.hireAnEmployee(ellen);
        sue.hireAnEmployee(fred);

        jack.hireAnEmployee(john);
        jack.hireAnEmployee(sue);
    }


    @Test
    public void shouldReturnUnsatisfiedEmployees() {

        List<String> unsatisfied =
                company.getUnsatisfiedEmployees()
                        .stream()
                        .map(Employee::getName)
                        .collect(Collectors.toList());

        assertEquals("[John, Sue, Giorgia, Fred]", unsatisfied.toString());
    }

}
