package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.BudgetOrientedManager;
import com.porcupine.javaclasses.Staff.CounterOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import com.porcupine.javaclasses.Staff.Manager;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;

public class BFSCompanyIteratorTest {

    private Company company;

    @Before
    public void setUp() {

        company = Company.getInstance();

        Manager maciek;
        Manager krzysiek;
        Employee wacek;
        Employee leszek;
        Employee grzesiek;
        Employee frodo;

        Manager zbyszek = new BudgetOrientedManager("Zbyszek", 11000, 20000);
        company.hireCEO(zbyszek);

        maciek = new BudgetOrientedManager("Maciek", 3500, 10000);
        krzysiek = new CounterOrientedManager("Krzysiek", 4000, 5);
        wacek = new Employee("Wacek", 1000);
        leszek = new Employee("Leszek", 2000);
        grzesiek = new Employee("Grzesiek", 1400);
        frodo = new Employee("Frodo", 1900);

        maciek.hireAnEmployee(wacek);
        maciek.hireAnEmployee(leszek);

        krzysiek.hireAnEmployee(frodo);

        zbyszek.hireAnEmployee(maciek);
        zbyszek.hireAnEmployee(grzesiek);
        zbyszek.hireAnEmployee(krzysiek);
    }

    @Test
    public void shouldIterateOverAll() {

        List<String> employeesNames = new LinkedList<>();

        for (Employee e : company)
            employeesNames.add(e.getName());

        assertEquals("[Zbyszek, Maciek, Grzesiek, Krzysiek, Wacek, Leszek, Frodo]", employeesNames.toString());
    }

    @Test
    public void shouldIterateOverSome() {

        List<String> employeesNames = new LinkedList<>();

        Predicate<Employee> predicate = employee -> employee.getSalary() > 1950;

        for (Iterator<Employee> it = new BFSCompanyIterator(predicate); it.hasNext();) {
            employeesNames.add(it.next().getName());
        }

        assertEquals("[Zbyszek, Maciek, Krzysiek, Leszek]", employeesNames.toString());
    }
}
