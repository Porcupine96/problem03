package com.porcupine.javaclasses;

import com.porcupine.javaclasses.Staff.CounterOrientedManager;
import com.porcupine.javaclasses.Staff.Employee;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CounterOrientedManagerTest {

    private CounterOrientedManager fred;
    private CounterOrientedManager george;
    private Employee jack;

    @Before
    public void setUp() {
        fred = new CounterOrientedManager("Fred", 10000, 2);
        george = new CounterOrientedManager("George", 10000, 0);
        jack = new Employee("Jack", 5000);
    }

    @Test
    public void shouldHireAnEmployee() {

        assertEquals("Welcome on board Jack!\n", fred.hireAnEmployee(jack));
        assertEquals(jack, fred.getEmployeeList().get(0));
    }

    @Test
    public void shouldNotHireAnEmployee() {

        assertEquals("Sorry, but I can't hire you.\n", george.hireAnEmployee(jack));
    }
}
